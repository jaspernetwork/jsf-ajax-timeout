package net.jaspernetwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsfSpringWebflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsfSpringWebflowApplication.class, args);
	}
}
