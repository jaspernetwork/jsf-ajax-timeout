package com.abc.module;


public class SampleXmlRequest implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2112095749387164224L;
	private String id;
	private String message;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
