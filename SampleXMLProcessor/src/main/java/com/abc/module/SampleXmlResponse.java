package com.abc.module;

public class SampleXmlResponse implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4943861263555824876L;

	private String id;
	private String message;
	private String error;
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
