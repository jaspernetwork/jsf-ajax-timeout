package com.abc;

// Generated Jun 30, 2011 11:51:44 AM by Hibernate Tools 3.2.2.GA


public class Change implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3438533704163760809L;
	private Integer type;
	private Detail detail;
	private short versionNumber;
	private String confirmationNumber;
	private boolean changed;// optioinal 

	private Boolean deleted; // must be yes or no  
 
	public Change() {
	}

}
