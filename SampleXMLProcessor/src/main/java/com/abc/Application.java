package com.abc;

// Generated Apr 2, 2013 1:44:29 PM by Hibernate Tools 3.4.0.CR1

import java.math.BigDecimal;

public class Application implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer type; // not null 
	private BigDecimal length; // nullable

	private Detail detail;
	private String confirmationNumber; //not null
	private boolean deleted;

	public Application() {
	}

}
