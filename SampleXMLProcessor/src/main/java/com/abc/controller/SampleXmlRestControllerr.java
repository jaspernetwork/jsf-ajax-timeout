package com.abc.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.abc.module.SampleXmlRequest;
import com.abc.module.SampleXmlResponse;

@RestController
public class SampleXmlRestControllerr {

	private static Logger logger = Logger
			.getLogger(SampleXmlRestControllerr.class.getName());

	public SampleXmlRestControllerr() throws Exception {

	}

	@RequestMapping(value = "/status/{employeeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<SampleXmlResponse> getAppSupp(
			@PathVariable("employeeId") String employeeId) {
		logger.debug("Querying employee " + employeeId);

		SampleXmlResponse response = new SampleXmlResponse();

		try {

			return new ResponseEntity<SampleXmlResponse>(response,
					HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e);
			return new ResponseEntity<SampleXmlResponse>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/xmlRest", method = RequestMethod.POST)
	public ResponseEntity<SampleXmlResponse> updateAppSupp(
			@RequestBody SampleXmlRequest request,
			UriComponentsBuilder ucBuilder) {

		SampleXmlResponse response = new SampleXmlResponse();
		HttpHeaders headers = new HttpHeaders();

		ResponseEntity<SampleXmlResponse> responseConflict = new ResponseEntity<SampleXmlResponse>(
				response, headers, HttpStatus.CONFLICT);

		ResponseEntity<SampleXmlResponse> responseCreated = new ResponseEntity<SampleXmlResponse>(
				response, headers, HttpStatus.CREATED);

		if (0 == 1)
			return new ResponseEntity<SampleXmlResponse>(HttpStatus.NOT_FOUND);

		if (1 == 1)
			return responseCreated;
		else
			return responseConflict;

	}
}
