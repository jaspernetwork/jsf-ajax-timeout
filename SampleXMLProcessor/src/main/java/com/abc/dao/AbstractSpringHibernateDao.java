package com.abc.dao;

import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

public abstract class AbstractSpringHibernateDao extends HibernateDaoSupport {

	//

	// @SuppressWarnings("unchecked")
	// public Code getCode(
	// final int codeId) {
	// Criteria criteria = getHibernateTemplate()
	// .getSessionFactory()
	// .getCurrentSession()
	// .createCriteria(
	// Codes.class);
	//
	// Codes code = null;
	// criteria.add(Restrictions.eq("codeId", BigDecimal.valueOf(dcId)));
	//
	// List<Code> rows = criteria
	// .list();
	// if (rows.size() > 0)
	// code = (Code) rows
	// .get(0);
	//
	// return code;
	// }

	public void flush() {
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();

	}

	public void commitTransaction() {
		getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getTransaction().commit();

	}

	public void rollbackTransaction() {
		getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getTransaction().rollback();
	}

	public void beginTransaction() {
		getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getTransaction().begin();
	}

	public void closeCurrentSession() {
		getHibernateTemplate().getSessionFactory().getCurrentSession().close();
	}

}