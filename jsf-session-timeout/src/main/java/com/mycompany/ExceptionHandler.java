
package com.mycompany;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.webflow.engine.FlowExecutionExceptionHandler;
import org.springframework.webflow.engine.RequestControlContext;
import org.springframework.webflow.engine.Transition;
import org.springframework.webflow.engine.ViewState;
import org.springframework.webflow.engine.support.DefaultTargetStateResolver;
import org.springframework.webflow.execution.FlowExecutionException;

@Component
public class ExceptionHandler implements FlowExecutionExceptionHandler {

	private static Logger logger = Logger
			.getLogger(ExceptionHandler.class.getName());

	public boolean canHandle(FlowExecutionException ex) {

		return true;

	}

	public void handle(FlowExecutionException ex,
			RequestControlContext context) {
		context.getMessageContext().addMessage(new MessageBuilder().error()
				.source(null).defaultText(ex.getMessage()).build());
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(ex.getMessage()));

		logger.error(ExceptionUtils.getFullStackTrace(ex));

		Object testState = context.getCurrentState();

		if (testState instanceof ViewState) {
			ViewState viewState = (ViewState) testState;
			try {
				viewState.getViewFactory().getView(context).render();
			} catch (IOException e) {
				// Properly handle rendering errors here
			}
		} else {

			context.execute(new Transition(
					new DefaultTargetStateResolver("systemException")));
		}

	}

}